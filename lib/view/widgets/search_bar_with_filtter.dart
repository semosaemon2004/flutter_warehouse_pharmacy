import 'package:flutter/material.dart';
import 'package:flutter_pharmacy_app/data/enums/search_type.dart';

class SearchBarWithFilter extends StatelessWidget {
  final TextEditingController controller;
  final Function onFilter;
  final SearchType searchType;

  SearchBarWithFilter(
      {required this.controller,
      required this.onFilter,
      required this.searchType});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
            ),
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: TextField(
              controller: controller,
              decoration: InputDecoration(
                prefixIcon: const Icon(Icons.search),
                hintText: 'Search...',
                suffixIcon: const Icon(Icons.mic),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide.none,
                ),
              ),
            ),
          ),
        ),
        TextButton(
          onPressed: () => onFilter(),
          child: Text(
            searchType == SearchType.medicine ? 'Category' : 'Medicine',
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
