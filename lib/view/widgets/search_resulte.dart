import 'package:flutter/material.dart';
import 'package:flutter_pharmacy_app/core/constant/colors.dart';
import 'package:flutter_pharmacy_app/data/enums/search_type.dart';
import 'package:flutter_pharmacy_app/data/model/category_search.dart';
import 'package:flutter_pharmacy_app/data/model/medicine_search.dart';

class SearchResults extends StatelessWidget {
  final List<MedicineSearch> medicines;
  final List<CategorySearch> categories;
  final SearchType searchType;

  SearchResults({
    required this.medicines,
    required this.categories,
    required this.searchType,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: searchType == SearchType.medicine
          ? medicines.length
          : categories.length,
      itemBuilder: (context, index) {
        if (searchType == SearchType.medicine) {
          return _buildMedicineItem(medicines[index]);
        } else {
          return _buildCategoryItem(categories[index]);
        }
      },
    );
  }

  Widget _buildMedicineItem(MedicineSearch medicine) {
    return ListTile(
      hoverColor: AppColors.darkerGreen,
      title: Text(medicine.title),
      subtitle: Text(medicine.type),
    );
  }

  Widget _buildCategoryItem(CategorySearch category) {
    return ListTile(
      title: Text(category.title),
    );
  }
}
