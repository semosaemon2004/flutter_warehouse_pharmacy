import 'package:flutter/material.dart';
import 'package:flutter_pharmacy_app/core/constant/colors.dart';
import 'package:flutter_pharmacy_app/view/screens/deatils_oreder.dart';

class Cardorder extends StatelessWidget {
  final int orderNumber;
  final double totalPrice;
  final String orderState;
  final String priceState;
  final DateTime orderDate;

  const Cardorder({
    Key? key,
    required this.orderNumber,
    required this.totalPrice,
    required this.orderState,
    required this.priceState,
    required this.orderDate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String formattedDate =
        "${orderDate.day}/${orderDate.month}/${orderDate.year}";

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DeatilsOrderScreen(orderId: orderNumber),
              ),
            );
          },
          child: Container(
            padding: const EdgeInsets.all(10),
            height: 150,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const Text(
                        "Order",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: AppColors.darkerGreen,
                        ),
                      ),
                      Text('Total: ${totalPrice.toStringAsFixed(2)} SYP'),
                      Text('Order State: $orderState'),
                      Text('Payment State: $priceState'),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      formattedDate,
                      style: const TextStyle(
                        color: AppColors.darkGreen,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const Icon(
                      Icons.arrow_forward_ios,
                      color: AppColors.darkerGreen,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
