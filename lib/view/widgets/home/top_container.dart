import 'package:flutter/material.dart';
import 'package:flutter_pharmacy_app/core/constant/colors.dart';
import 'package:flutter_pharmacy_app/data/enums/search_type.dart';
import 'package:flutter_pharmacy_app/view/screens/cart_screen.dart';
import 'package:flutter_pharmacy_app/view/widgets/search_bar_with_filtter.dart';

class TopContainer extends StatelessWidget {
  final TextEditingController searchBarController;
  final Function onFilter;
  final SearchType searchType;

  const TopContainer({
    required this.searchBarController,
    required this.onFilter,
    required this.searchType,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: AppColors.primary,
          borderRadius: BorderRadius.circular(30),
        ),
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Hello,',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                      fontWeight: FontWeight.bold),
                ),
                CircleAvatar(
                  backgroundColor: Colors.grey[350],
                  child: IconButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                         return Cartpage();
                      }));
                    },
                    icon: Icon(Icons.shopping_cart_outlined),
                    color: Colors.teal[300],
                  ),
                )
              ],
            ),
            const SizedBox(height: 5),
            Container(
              alignment: Alignment.centerLeft,
              child: const Text(
                'Welcom to your app',
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
            const SizedBox(height: 20),
            SearchBarWithFilter(
              controller: searchBarController,
              onFilter: onFilter,
              searchType: searchType,
            ),
          ],
        ),
      ),
    );
  }
}
