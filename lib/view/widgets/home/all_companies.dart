import 'package:flutter/material.dart';
import 'package:flutter_pharmacy_app/services/all_services.dart';
import 'package:flutter_pharmacy_app/view/screens/categories_screen.dart';

class AllCompanies extends StatefulWidget {
  const AllCompanies({super.key});

  @override
  State<AllCompanies> createState() => _AllCompaniesState();
}

class _AllCompaniesState extends State<AllCompanies> {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 20, top: 10),
          alignment: Alignment.centerLeft,
          child: const Text(
            "All Companies",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        FutureBuilder<List>(
          future: Services().getcompany(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return GridView.count(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                mainAxisSpacing: screenSize.height * 0.005,
                crossAxisSpacing: screenSize.width * 0.01,
                crossAxisCount: screenSize.width > 600 ? 3 : 2,
                children: List.generate(
                  snapshot.data!.length,
                  (i) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return CategoriesScreen(
                                  x: snapshot.data![i]['id'],
                                );
                              },
                            ),
                          );
                        },
                        child: Card(
                          elevation: 9,
                          color: Colors.green[50],
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18),
                          ),
                          child: Column(
                            children: [
                              SizedBox(height: 20),
                              Image.network(
                                "http://10.0.2.2:8000/${snapshot.data![i]['photo']}",
                                height: 100,
                                width: 120,
                              ),
                              Text(
                                "${snapshot.data![i]['Company_Name']}",
                                style: const TextStyle(
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              );
            }
            return CircularProgressIndicator();
          },
        ),
      ],
    );
  }
}
