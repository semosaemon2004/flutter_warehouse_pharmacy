import 'package:flutter/material.dart';
import 'package:flutter_pharmacy_app/core/constant/colors.dart';
import 'package:flutter_pharmacy_app/services/all_services.dart';
import 'package:flutter_pharmacy_app/view/widgets/cart_widget.dart';

class Cartpage extends StatefulWidget {
  Cartpage({Key? key}) : super(key: key);

  @override
  State<Cartpage> createState() => _CartpageState();
}

class _CartpageState extends State<Cartpage> {
  final Services services = Services();
  List<Map<String, dynamic>> cartItems = [];

  @override
  void initState() {
    super.initState();
    _showCartItems();
  }

  void _showCartItems() async {
    try {
      cartItems = await services.showCart();
      print('Cart Items: $cartItems');
      setState(() {});
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Failed to retrieve cart items: $e'),
        ),
      );
    }
  }

  double getTotalPrice() {
    double totalPrice = 0;
    for (var item in cartItems) {
      double price = double.tryParse(item['Price'].toString()) ?? 0;
      int quantity = int.tryParse(item['Quantity'].toString()) ?? 0;

      print(
          "Item: ${item['Scientific_Name']}, Price: $price, Quantity: $quantity");

      totalPrice += price * quantity;
    }
    return totalPrice;
  }

  void updateQuantity(int index, int newQuantity) {
    setState(() {
      cartItems[index]['Quantity'] = newQuantity.toString();
    });
  }

  Future<void> confirmOrder() async {
    if (cartItems.isNotEmpty) {
      double finalPrice =
          getTotalPrice(); // Replace with your method to calculate the total price
      List<Map<String, dynamic>> quantityPrice = cartItems.map((item) {
        return {
          'Product_id': item[
              'Product_id'], // Ensure you have 'Product_id' in your item map
          'Price': item['Price'],
          'Quantity': item['Quantity'],
        };
      }).toList();

      try {
        var response = await services.createOrder(finalPrice, quantityPrice);
        print('Order created: $response');

        // Clear the cart items after successful order creation.
        setState(() {
          cartItems.clear();
        });

        // Show a snackbar with the order ID.
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Order sent")),
        );
      } catch (e) {
        // Log the error or use a developer console for a more detailed error report.
        print(e.toString());

        // Show a snackbar with the error message.
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Order failed: ${e.toString()}")),
        );
      }
    } else {
      print("Cart is empty");
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text("Cart is empty")),
      );
    }
  }

  Future<void> deletetFromCart(int productId, int index) async {
    try {
      await services.deleteProductFromCart(productId);
      setState(() {
        cartItems.removeAt(index);
      });
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text("Item removed from cart")),
      );
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Failed to delete item from cart: $e')),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double totalPrice = getTotalPrice();

    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            color: AppColors.primary,
            child: Align(
              alignment: Alignment(0, -0.83),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Colors.white,
                      ),
                    ),
                    const Text(
                      'Cart',
                      style: TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 110),
            child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(60)),
              ),
              height: double.infinity,
              child: Column(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20, left: 10),
                      child: Container(
                        width: 380,
                        child: ListView.builder(
                            itemCount: cartItems.length,
                            itemBuilder: (context, i) {
                              double price = double.tryParse(
                                      cartItems[i]['Price'].toString()) ??
                                  0;

                              return Dismissible(
                                key: Key(cartItems[i]['id'].toString()),
                                background: Container(
                                  color: Colors.red,
                                  child: const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Icon(Icons.delete, color: Colors.white),
                                        Text(
                                          'Remove',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                direction: DismissDirection.endToStart,
                                onDismissed: (direction) {
                                  if (cartItems[i].containsKey('id') &&
                                      cartItems[i]['id'] is int) {
                                    int productId = cartItems[i]['id'];
                                    deletetFromCart(productId, i);
                                  } else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                          content: Text(
                                              "Error: Cannot delete item without a valid product ID")),
                                    );
                                  }
                                },
                                child: Cartwidget(
                                  name: cartItems[i]['Scientific_Name'],
                                  image:
                                      "http://10.0.2.2:8000/${cartItems[i]['photo']}",
                                  price: price,
                                  onQuantityChange: (newQuantity) {
                                    updateQuantity(i, newQuantity);
                                  },
                                ),
                              );
                            }),
                      ),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 100,
                    color: Colors.white,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 8.0, left: 10),
                          child: Row(
                            children: [
                              Text(
                                'TotalPrice: $totalPrice SYP',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: ElevatedButton(
                            onPressed: confirmOrder,
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              primary: AppColors.darkerGreen,
                              fixedSize: Size(270, 45),
                            ),
                            child: const Text(
                              'Check Out',
                              style:
                                  TextStyle(fontSize: 22, color: Colors.white),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
