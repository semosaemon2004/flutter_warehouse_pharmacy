import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_pharmacy_app/core/constant/colors.dart';
import 'package:flutter_pharmacy_app/core/constant/linkapi.dart';
import 'package:flutter_pharmacy_app/view/screens/auth/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogoutController {
  Future<void> logout(BuildContext context) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => const Center(
        child: CircularProgressIndicator(
          color: AppColors.darkerGreen,
          backgroundColor: AppColors.lightGreen,
        ),
      ),
    );

    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('authToken');

      if (token != null) {
        var response = await http.post(
          Uri.parse(linkLogout),
          headers: <String, String>{
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $token',
          },
        );

        if (response.statusCode == 200 || response.statusCode == 201) {
          print('Logout successful');
        } else {
          print('Error during logout: ${response.statusCode}');
        }
      }

      await prefs.clear();

      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => const LoginScreen()),
        (Route<dynamic> route) => false,
      );
    } catch (e) {
      print("Logout Error: $e");
      Navigator.of(context).pop();
    }
  }
}
