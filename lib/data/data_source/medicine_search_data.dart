import 'package:flutter_pharmacy_app/data/model/medicine_search.dart';

final allMedicine = <MedicineSearch>[
  const MedicineSearch(
    id: 1,
    title: 'Aspirin',
    type: 'Nonsteroidal anti-inflammatory drug.',
  ),
  const MedicineSearch(
    id: 2,
    title: 'Amoxicillin',
    type: 'Penicillin antibiotic.',
  ),
  const MedicineSearch(
    id: 3,
    title: 'Metformin',
    type: 'Biguanide.',
  ),
  const MedicineSearch(
    id: 4,
    title: 'Lisinopril',
    type: 'Angiotensin-converting enzyme (ACE) inhibitor.',
  ),
  const MedicineSearch(
    id: 5,
    title: 'Simvastatin',
    type: 'Statin.',
  ),
];
