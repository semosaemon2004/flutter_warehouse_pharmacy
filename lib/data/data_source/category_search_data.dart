import 'package:flutter_pharmacy_app/data/model/category_search.dart';

final allCategories = <CategorySearch>[
  const CategorySearch(
    id: 1,
    title: 'Pain Relief',
  ),
  const CategorySearch(
    id: 2,
    title: 'Antibiotics',
  ),
  const CategorySearch(
    id: 3,
    title: 'Diabetes',
  ),
  const CategorySearch(
    id: 4,
    title: 'Heart',
  ),
  const CategorySearch(
    id: 5,
    title: 'Vitamins',
  ),
];
