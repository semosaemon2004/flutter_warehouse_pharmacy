class MedicineSearch {
  final int id;
  final String title;
  final String type;

  const MedicineSearch({
    required this.id,
    required this.type,
    required this.title,
  });

  factory MedicineSearch.fromJson(Map<String, dynamic> json) => MedicineSearch(
        id: json['id'],
        type: json['type'],
        title: json['title'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'type': type,
      };
}
