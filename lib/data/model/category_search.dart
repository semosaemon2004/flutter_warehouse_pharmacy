class CategorySearch {
  final int id;
  final String title;
  

  const CategorySearch({
    required this.id,
    required this.title,
  });

  factory CategorySearch.fromJson(Map<String, dynamic> json) => CategorySearch(
        id: json['id'],
        title: json['title'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
      };
}
