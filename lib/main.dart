import 'package:flutter/material.dart';
import 'package:flutter_pharmacy_app/view/screens/onboarding.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pharmacy App',
      debugShowCheckedModeBanner: false,
      home: OnBoardingScreen(),
    );
  }
}
